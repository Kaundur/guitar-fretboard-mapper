import React from 'react';


import './notePickerNote.css';

type NotePickerNoteProps = {
    note: string,
    toggled: boolean,
    updateSelectedNoteCallback: (note: string) => void
}

export const NotePickerNote: React.FC<NotePickerNoteProps> = (props) => {



    // const {useNoteToggle} =

    // const noteClick = () => useNoteToggle(props.note)




    let className = 'note-picker-note';
    if (props.toggled) {
        className += ' note-picker-note-selected';
    }

    const selectorClick = () => {
        props.updateSelectedNoteCallback(props.note);
    };


    return (
        <div className={className} onClick={selectorClick}>
            {props.note}
        </div>
    )
};

