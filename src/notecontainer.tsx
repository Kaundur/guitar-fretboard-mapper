
import React from 'react';

import './NoteContainer.css';

type NoteContainerProps = {
    fretStep: number;
    stringStep: number;
    fretNumber: number;
    stringNumber: number;
    note: string
    active: boolean
}

export const NoteContainer: React.FC<NoteContainerProps> = (props) => {

    const calculateTopOffset = () => {
        return props.stringNumber*props.stringStep + props.stringStep - 13.5; // -5 as note is 10px high
    };

    const calculateLeftOffset = () => {
        // -5 as note is 10px wide, step/2.0 to put the note in the middle between frets
        return props.fretNumber*props.fretStep + props.fretStep/2.0 - 13.5;
    };

    const renderNote = () => {
        if (props.active) {
            return (
                <div className="note" style={{'top': calculateTopOffset(), 'left': calculateLeftOffset()}}>
                    {props.note}
                </div>
            )
        }
        else {
            return (
                <div></div>
            )
        }
    }

    return (
        <div>
            {renderNote()}
        </div>
    )
};

