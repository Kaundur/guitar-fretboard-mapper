import React from 'react';
import {NotePickerNote} from "./notePickerNote";

import './NotePickerContainer.css';

type NotePickerContainerProps = {
    selectedNotes: {[key: string]: boolean},
    updateSelectedNoteCallback: (note: string) => void,
}

export const NotePickerContainer: React.FC<NotePickerContainerProps> = (props) => {

    const renderNotes = () => {
        let notePickers = [];

        for (let note in props.selectedNotes) {
            let toggled = props.selectedNotes[note];
            notePickers.push(
                <NotePickerNote note={note} toggled={toggled} updateSelectedNoteCallback={props.updateSelectedNoteCallback}/>
            )
        }
        return notePickers
    };

    return (
        <div className="note-picker-container">
            {renderNotes()}
        </div>
    )
};

