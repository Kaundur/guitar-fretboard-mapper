
import React from 'react';
//
import './FretBoard.css';
import {StringContainer} from "./stringcontainer";
import {FretContainer} from "./fretContainer";
//
// import Square from './square;



type FretBoardProps = {
    numStrings: number;
    numFrets: number;
    tuning: string[];
    notes: {[key: string]: boolean};
    updateTuningCallback: (stringIndex: number, note: string) => void;
}

export const FretBoard: React.FC<FretBoardProps> = (props) => {

    let fullHeight = 200;
    let fretStep = 50;

    const generateFrets = () => {
        let frets = [];
        for (let i=0; i < props.numFrets; i++) {
            frets.push(<FretContainer width={fretStep} fretNumber={i}/>)
        }
        return frets;

    };

    const generateStrings = () => {
        let strings = [];

        const heightStep = fullHeight/(props.tuning.length+1);

        for (let i=0; i<props.tuning.length; i++) {
            strings.push(
                <StringContainer
                    height={heightStep}
                    stringNumber={i}
                    tuning={props.tuning[i]}
                    fretStep={fretStep}
                    numFrets={props.numFrets}
                    notes={props.notes}
                    updateTuningCallback={props.updateTuningCallback}
                />
            )
        };
        return strings;
    };

    return (
        <div className="fret-board" style={{'height': fullHeight}}>
            {generateFrets()}
            {generateStrings()}
        </div>
    )
};

