import React from 'react';
import './App.css';
import {FretBoard} from "./fretboard";
import {NotePickerContainer} from "./notePickerContainer";
import {ArrowButton} from "./button/arrowButton";


const NOTES: string[] = ['A', 'A#/Bb', 'B', 'C', 'C#/Db', 'D', 'D#/Eb', 'E', 'F', 'F#/Gb', 'G', 'G#/Ab'];

class App extends React.Component  {

    state: {
        selectedAscending: {[key: string]: boolean}
        tuning: string[]
    };
    constructor (props: any) {
        super(props);
        this.state = {
            selectedAscending: {
                'A': false,
                'A#/Bb': false,
                'B': false,
                'C': false,
                'C#/Db': false,
                'D': true,
                'D#/Eb': false,
                'E': false,
                'F': false,
                'F#/Gb': false,
                'G': false,
                'G#/Ab': false
            },
            tuning: ['E', 'B', 'G', 'D', 'A', 'E']
        };

        this.updateSelectedNote = this.updateSelectedNote.bind(this);
        this.shiftKey = this.shiftKey.bind(this);
        this.setTuning = this.setTuning.bind(this);

    }

    setTuning (stringIndex: number, note: string): void {
        let newState = this.state;

        newState.tuning[stringIndex] = note;
        this.setState(newState);
    }

    shiftKey (step: number): void {
        let newState = this.state;

        let newSelectedAscending: {[key: string]: boolean} = {};
        for (let note in newState.selectedAscending) {
            newSelectedAscending[note] = false;
        }

        for (let note in newState.selectedAscending) {
            if (newState.selectedAscending[note]) {
                let noteIndex = NOTES.indexOf(note) + step;

                if (noteIndex < 0) {
                    noteIndex = NOTES.length-noteIndex-2;
                }

                let shiftedIndex = (noteIndex)%NOTES.length;
                newSelectedAscending[NOTES[shiftedIndex]] = true;
            }
        }

        newState.selectedAscending = newSelectedAscending;
        this.setState(newState);
    }

    updateSelectedNote (note: string): void {
        let newState = this.state;
        newState.selectedAscending[note] = !newState.selectedAscending[note];
        this.setState(newState);
    };


    render() {

        return (
            <div className="App">
                <div className="board-container">
                    <FretBoard
                        numStrings={6}
                        numFrets={23}
                        tuning={this.state.tuning}
                        updateTuningCallback={this.setTuning}
                        notes={this.state.selectedAscending}
                    ></FretBoard>
                </div>


                <div>

                    <div style={{'display': 'inline-block'}}>
                        <NotePickerContainer selectedNotes={this.state.selectedAscending} updateSelectedNoteCallback={this.updateSelectedNote}/>
                    </div>
                </div>
                <div style={{'height': '100px'}}>


                        <div style={{'margin': '10px', 'textAlign': 'center'}}>
                            Shift Key
                        </div>

                    <div style={{'display': 'inline-block'}}>
                        <div onClick={() => this.shiftKey(1)}>
                            <ArrowButton isUp={true} />
                        </div>
                        <div onClick={() => this.shiftKey(-1)}>
                            <ArrowButton isUp={false}/>
                        </div>
                    </div>

                </div>

            </div>
        );
    }
}

export default App;
