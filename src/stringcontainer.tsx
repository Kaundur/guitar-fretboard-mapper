
import React from 'react';

import './StringContainer.css';
import {NoteContainer} from "./notecontainer";
import {NotePicker} from "./button/notePicker";

const NOTES: string[] = ['A', 'A#/Bb', 'B', 'C', 'C#/Db', 'D', 'D#/Eb', 'E', 'F', 'F#/Gb', 'G', 'G#/Ab'];
// const NOTES: string[] = ['A', 'A#', 'B', 'C', 'C#', 'D', 'D#', 'E', 'F', 'F#', 'G', 'G#'];

type StringContainerProps = {
    height: number;
    stringNumber: number;
    fretStep: number;
    numFrets: number;
    tuning: string;
    notes: {[key: string]: boolean};
    updateTuningCallback: (stringIndex: number, note: string) => void;
}

export const StringContainer: React.FC<StringContainerProps> = (props) => {

    const generateNotes = () => {
        const startingNoteIndex: number = NOTES.indexOf(props.tuning);

        let stringNotes = [];
        for (let i=0; i<props.numFrets; i++) {

            const currentNoteIndex: number = (startingNoteIndex + i)%NOTES.length;
            let currentNote = NOTES[currentNoteIndex];

            let isActive:boolean = false;


            if (currentNote in props.notes && props.notes[currentNote]) {
                isActive = true;
            }


            // if (props.notes.indexOf(NOTES[currentNoteIndex]) >= 0) {
            //     isActive = true;
            // }



            stringNotes.push(<NoteContainer
                fretNumber={i}
                fretStep={props.fretStep}
                stringNumber={props.stringNumber}
                stringStep={props.height}
                note={NOTES[currentNoteIndex]}
                active={isActive}
            ></NoteContainer>)
        }
        return stringNotes;
    };

    return (
        <div>
            <div className="note-drop-down-picker" style={{'top': props.height*(props.stringNumber + 1) - 10}}>
                <NotePicker
                    selectedNote={props.tuning} updateTuningCallback={props.updateTuningCallback}
                    stringNumber={props.stringNumber}
                />
            </div>
            <div className="string" style={{'top': props.height*(props.stringNumber + 1)}}></div>
            {generateNotes()}
        </div>
    )
};

