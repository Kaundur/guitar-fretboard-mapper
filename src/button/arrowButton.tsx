import React from 'react';

import './Button.css';

type ArrowButtonProps = {
    isUp: boolean
}

export const ArrowButton: React.FC<ArrowButtonProps> = (props) => {

    let className = 'arrow-button';
    if (props.isUp) {
        className += ' up-arrow-button';
    }
    else {
        className += ' down-arrow-button';
    }

    return (


        <i className={className}></i>
    )
};

