import React from 'react';

import './Button.css';

type NotePickerProps = {
    selectedNote: string;
    stringNumber: number;
    updateTuningCallback: (stringIndex: number, note: string) => void;
}

const NOTES: string[] = ['A', 'A#/Bb', 'B', 'C', 'C#/Db', 'D', 'D#/Eb', 'E', 'F', 'F#/Gb', 'G', 'G#/Ab'];

export const NotePicker: React.FC<NotePickerProps> = (props) => {

    const generateOptions = () => {
        let options = [];
        for (let note of NOTES) {
            let selected = false;

            if (note === props.selectedNote) {
                selected = true;
            }

            options.push(
                <option value={note} selected={selected}>{note}</option>
            )
        }
        return options;
    };

    const change = (event: any) => {
        console.log(event.target.value);
        props.updateTuningCallback(props.stringNumber, event.target.value);
        // onChange={() => props.updateTuningCallback(props.stringNumber, 'A')}
    }

    return (
        <div>
            <select onChange={change} >
                {generateOptions()}
            </select>
        </div>
    )
};
