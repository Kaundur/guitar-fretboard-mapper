
import React from 'react';

import './FretContainer.css';

type FretContainerProps = {
    width: number;
    fretNumber: number;
}

export const FretContainer: React.FC<FretContainerProps> = (props) => {

    const renderDot = () => {
        // Single dots
        // 3, 5, 7, 9, -  15, 17, 19, 21
        // Double dots 12

        // Plus 1 because its not an index
        const fret: number = props.fretNumber+1;

        const singleDots: number[] = [3, 5, 7, 9, 15, 17, 19, 21];

        if (fret === 12) {
            return (<div>
                <div className="fret-double-dot" style={{'left': props.width/2.0, 'top': '28%'}}></div>
                <div className="fret-double-dot" style={{'left': props.width/2.0, 'bottom': '26%'}}></div>
            </div>)
        }
        else if (singleDots.indexOf(fret) >= 0) {
            return (<div className="fret-dot" style={{'left': props.width/2.0}}></div>)
        }

        return (<div></div>)
    };

    const renderFretNumber = () => {
        if (props.fretNumber > 0) {
            return (
                <div className="fret-number">{props.fretNumber}</div>
            )
        }
    };

    return (
        <div className="fret" style={{'left': props.width*(props.fretNumber + 1)}}>
            {renderDot()}
            {renderFretNumber()}
        </div>
    )
};

